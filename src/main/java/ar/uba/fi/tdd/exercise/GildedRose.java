
package ar.uba.fi.tdd.exercise;

class GildedRose {

    private static final int MAX_QUALITY = 50;
    private static final int TEN_DAYS = 10;
    private static final int FIVE_DAYS = 5;

    private static final int NORMAL_ITEM_QUALITY_VARIATION = -1;
    private static final int BACKSTAGE_PASSES_QUALITY_VARIATION = 1;

    private static final String AGED_BRIE = "aged brie";
    private static final String BACKSTAGE_PASSES = "backstage passes";
    private static final String SULFURAS = "sulfuras";
    private static final String CONJURED = "conjured";

    Item[] items;

    public GildedRose(Item[] _items) {
        items = _items;
    }

    public void updateQuality() {

        for (Item item : items) {

            updateSellInOfItem(item);

            switch (itemType(item)) {
                case AGED_BRIE:
                    item.quality = addBetween50and0(item.quality, agedBrieQualityVariation(item));
                    break;
                case SULFURAS:
                    break;
                case BACKSTAGE_PASSES:
                    item.quality = addBetween50and0(item.quality, backstagePassesQualityVariation(item));
                    break;
                case CONJURED:
                    item.quality = addBetween50and0(item.quality, conjuderQualityVariation(item));
                    break;
                default:
                    item.quality = addBetween50and0(item.quality, normalItemQualityVariation(item));
            }
        }
    }

    private void updateSellInOfItem(Item item) {
        if (!item.Name.toLowerCase().contains(SULFURAS)) item.sellIn--;
    }

    private String itemType(Item item) {
        if (item.Name.toLowerCase().contains(SULFURAS)) return SULFURAS;
        if (item.Name.toLowerCase().contains(BACKSTAGE_PASSES)) return BACKSTAGE_PASSES;
        return item.Name.toLowerCase();
    }

    private int agedBrieQualityVariation(Item agedBrie) {
        return -normalItemQualityVariation(agedBrie);
    }

    private int backstagePassesQualityVariation(Item backstagePasses) {

        if (backstagePasses.sellIn < 0) {
            return -backstagePasses.quality;
        } else {
            if (backstagePasses.sellIn < FIVE_DAYS) {
                return 3 * BACKSTAGE_PASSES_QUALITY_VARIATION;
            } else if (backstagePasses.sellIn < TEN_DAYS) {
                return 2 * BACKSTAGE_PASSES_QUALITY_VARIATION;
            } else {
                return BACKSTAGE_PASSES_QUALITY_VARIATION;
            }
        }
    }

    private int conjuderQualityVariation(Item conjured) {
        return 2*normalItemQualityVariation(conjured);
    }

    private int normalItemQualityVariation(Item normalItem){

        if(normalItem.sellIn < 0) {
            return 2 * NORMAL_ITEM_QUALITY_VARIATION;
        }else{
            return NORMAL_ITEM_QUALITY_VARIATION;
        }
    }

    private int addBetween50and0(int aNumber, int otherNumber) {
        int result = aNumber + otherNumber;

        if (result < 0) {
            return 0;
        } else {
            return Math.min(result, MAX_QUALITY);
        }
    }
}

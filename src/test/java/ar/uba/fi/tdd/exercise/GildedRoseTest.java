package ar.uba.fi.tdd.exercise;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class GildedRoseTest {

	@Test
	public void NormalItemQualityDecrease() {
		Item[] items = new Item[] { new Item("Item", 10, 12) };
		GildedRose app = new GildedRose(items);

		app.updateQuality();

		assertThat(11).isEqualTo(app.items[0].quality);
	}

	@Test
	public void NormalItemSellInDecrease() {
		Item[] items = new Item[] { new Item("Item", 10, 12) };
		GildedRose app = new GildedRose(items);

		app.updateQuality();

		assertThat(9).isEqualTo(app.items[0].sellIn);
	}

	@Test
	public void NormalItemQualityDecreaseTwiceAfterSellInIs0() {
		Item[] items = new Item[] { new Item("Item", 0, 12) };
		GildedRose app = new GildedRose(items);

		app.updateQuality();

		assertThat(10).isEqualTo(app.items[0].quality);
	}

	@Test
	public void NormalItemQualityDecreaseTwiceAfterSellInIsUnder0() {
		Item[] items = new Item[] { new Item("Item", -1, 12) };
		GildedRose app = new GildedRose(items);

		app.updateQuality();

		assertThat(10).isEqualTo(app.items[0].quality);
	}

	@Test
	public void NormalItemQualityDoseNotGoUnder0() {
		Item[] items = new Item[] { new Item("Item", 1, 0) };
		GildedRose app = new GildedRose(items);

		app.updateQuality();

		assertThat(0).isEqualTo(app.items[0].quality);
	}

	@Test
	public void NormalItemQualityDoseNotGoUnder0IfSellInIs0() {
		Item[] items = new Item[] { new Item("Item", 0, 0) };
		GildedRose app = new GildedRose(items);

		app.updateQuality();

		assertThat(0).isEqualTo(app.items[0].quality);
	}

	@Test
	public void AgedBrieQualityIncrease() {
		Item[] items = new Item[] { new Item("Aged Brie", 10, 0) };
		GildedRose app = new GildedRose(items);

		app.updateQuality();

		assertThat(1).isEqualTo(app.items[0].quality);
	}

	@Test
	public void AgedBrieQualityDoseNotSurpass50() {
		Item[] items = new Item[] { new Item("Aged Brie", 10, 50) };
		GildedRose app = new GildedRose(items);

		app.updateQuality();

		assertThat(50).isEqualTo(app.items[0].quality);
	}

	@Test
	public void AgedBrieQualityIncreaseTwiceAfterSellIn() {
		Item[] items = new Item[] { new Item("Aged Brie", 0, 0) };
		GildedRose app = new GildedRose(items);

		app.updateQuality();

		assertThat(2).isEqualTo(app.items[0].quality);
	}

	@Test
	public void SulfurasQualityIsNotAltered() {
		Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 10, 80) };
		GildedRose app = new GildedRose(items);

		app.updateQuality();

		assertThat(80).isEqualTo(app.items[0].quality);
	}

	@Test
	public void SulfurasSellInIsNotAltered() {
		Item[] items = new Item[] { new Item("Sulfuras, Hand of Ragnaros", 10, 80) };
		GildedRose app = new GildedRose(items);

		app.updateQuality();

		assertThat(10).isEqualTo(app.items[0].sellIn);
	}

	@Test
	public void BackstagePassesQualityIncreaseByOneWhenSellInIsGreater11() {
		Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 11, 20) };
		GildedRose app = new GildedRose(items);

		app.updateQuality();

		assertThat(21).isEqualTo(app.items[0].quality);
	}

	@Test
	public void BackstagePassesQualityIncreaseByTwoWhenSellInIs10() {
		Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 10, 20) };
		GildedRose app = new GildedRose(items);

		app.updateQuality();

		assertThat(22).isEqualTo(app.items[0].quality);
	}

	@Test
	public void BackstagePassesQualityIncreaseByTwoWhenSellInIs6() {
		Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 6, 20) };
		GildedRose app = new GildedRose(items);

		app.updateQuality();

		assertThat(22).isEqualTo(app.items[0].quality);
	}

	@Test
	public void BackstagePassesQualityIncreaseByThreeWhenSellInIs5() {
		Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 5, 20) };
		GildedRose app = new GildedRose(items);

		app.updateQuality();

		assertThat(23).isEqualTo(app.items[0].quality);
	}

	@Test
	public void BackstagePassesQualityIncreaseByThreeWhenSellInIs1() {
		Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 1, 20) };
		GildedRose app = new GildedRose(items);

		app.updateQuality();

		assertThat(23).isEqualTo(app.items[0].quality);
	}

	@Test
	public void BackstagePassesQualityIs0IfSellInIs0() {
		Item[] items = new Item[] { new Item("Backstage passes to a TAFKAL80ETC concert", 0, 20) };
		GildedRose app = new GildedRose(items);

		app.updateQuality();

		assertThat(0).isEqualTo(app.items[0].quality);
	}

	@Test
	public void ConjuredItemQualityDecrease() {
		Item[] items = new Item[] { new Item("Conjured", 10, 12) };
		GildedRose app = new GildedRose(items);

		app.updateQuality();

		assertThat(10).isEqualTo(app.items[0].quality);
	}

	@Test
	public void ConjuredItemSellInDecrease() {
		Item[] items = new Item[] { new Item("Conjured", 10, 12) };
		GildedRose app = new GildedRose(items);

		app.updateQuality();

		assertThat(9).isEqualTo(app.items[0].sellIn);
	}

	@Test
	public void ConjuredQualityDecreaseTwiceAfterSellInIs0() {
		Item[] items = new Item[] { new Item("Conjured", 0, 12) };
		GildedRose app = new GildedRose(items);

		app.updateQuality();

		assertThat(8).isEqualTo(app.items[0].quality);
	}

	@Test
	public void ConjuredDecreaseTwiceAfterSellInIsUnder0() {
		Item[] items = new Item[] { new Item("Conjured", -1, 12) };
		GildedRose app = new GildedRose(items);

		app.updateQuality();

		assertThat(8).isEqualTo(app.items[0].quality);
	}

	@Test
	public void ConjuredQualityDoseNotGoUnder0() {
		Item[] items = new Item[] { new Item("Conjured", 1, 0) };
		GildedRose app = new GildedRose(items);

		app.updateQuality();

		assertThat(0).isEqualTo(app.items[0].quality);
	}

	@Test
	public void ConjuredQualityDoseNotGoUnder0IfSellInIs0() {
		Item[] items = new Item[] { new Item("Conjured", 0, 0) };
		GildedRose app = new GildedRose(items);

		app.updateQuality();

		assertThat(0).isEqualTo(app.items[0].quality);
	}

}
